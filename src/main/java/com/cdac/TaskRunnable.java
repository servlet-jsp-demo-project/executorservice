package com.cdac;

public class TaskRunnable implements Runnable {

	private String message;
	private Integer waittime;
	private Integer loop;

	public TaskRunnable(String message, Integer loop, Integer waittime) {
		this.waittime = waittime * 1000;
		this.loop = loop;
		this.message = message;
	}

	public void run() {
		try {
			doWork();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doWork() throws InterruptedException {
		for (int i = 1; i <= loop; i++) {
			System.out.println(message + " : " + Thread.currentThread().getName());
			Thread.sleep(this.waittime);
		}

	}

}
