package com.cdac;

import java.util.concurrent.Callable;

public class TaskCallable implements Callable<String> {

	private String message;
	private Integer waittime;
	private Integer loop;

	public TaskCallable(String message, Integer loop, Integer waittime) {
		this.waittime = waittime * 1000;
		this.loop = loop;
		this.message = message;
	}

	public String call() throws Exception {
		return doWork();
	}

	public String doWork() throws InterruptedException {
		for (int i = 1; i <= loop; i++) {
			System.out.println(message + " : " + Thread.currentThread().getName());
			Thread.sleep(this.waittime);
		}
		return "Done "+message;

	}

}
