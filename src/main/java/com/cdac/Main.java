package com.cdac;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/*
 * https://jenkov.com/tutorials/java-util-concurrent/executorservice.html
 */
public class Main {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(3);

		List<Callable<String>> taskList = new ArrayList<Callable<String>>();
		taskList.add(new TaskCallable("Shivam", 2, 2));
		taskList.add(new TaskCallable("Vardhman", 4, 2));
		taskList.add(new TaskCallable("Ashwini", 3, 2));

		List<Future<String>> futureList = executorService.invokeAll(taskList);

		for (Future<String> future : futureList) {
			System.out.println(future.get());
		}
		executorService.shutdown();
	}
	/*
	 * Output : Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Done Shivam
	 * 
	 * Done Vardhman
	 * 
	 * Done Ashwini
	 */

	public static void main9(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(3);

		List<Callable<String>> taskList = new ArrayList<Callable<String>>();
		taskList.add(new TaskCallable("Shivam", 10, 2));
		taskList.add(new TaskCallable("Vardhman", 7, 2));
		taskList.add(new TaskCallable("Ashwini", 3, 2));

		String result = executorService.invokeAny(taskList);
		System.out.println(result);
		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-3
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Done Ashwini
	 */

	public static void main8(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(1);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		Future<String> futureShivam = executorService.submit(new TaskCallable("Shivam", 3, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		Future<String> futureVardhman = executorService.submit(new TaskCallable("Vardhman", 3, 2));

		System.out.println(futureShivam.get());
		System.out.println(futureVardhman.get());

		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Done Shivam
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Done Vardhman
	 */

	public static void main7(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(2);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		Future futureShivam = executorService.submit(new TaskRunnable("Shivam", 10, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		Future futureVardhman = executorService.submit(new TaskRunnable("Vardhman", 3, 2));

		// Task is to print "Ashwini" for 3 times at interval of 2 second
		Future futureAshwini = executorService.submit(new TaskRunnable("Ashwini", 5, 2));

		/*
		 * Waiting to Shivam print task to complete
		 * 
		 * Unless task is completed program execution will be halt here
		 */
		System.out.println(futureShivam.get());

		/*
		 * Waiting to Vardhman print task to complete
		 */
		System.out.println(futureVardhman.get());

		executorService.shutdown();
	}
	/*
	 * Output : Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * null
	 * 
	 * null
	 */

	public static void main6(String[] args) throws InterruptedException {

		ExecutorService executorService = Executors.newFixedThreadPool(2);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Shivam", 3, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Vardhman", 3, 2));

		// Task is to print "Ashwini" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Ashwini", 3, 2));

		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-2
	 * 
	 * Ashwini : pool-1-thread-2
	 */

	public static void main5(String[] args) throws InterruptedException {

		ExecutorService executorService = Executors.newFixedThreadPool(3);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Shivam", 3, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Vardhman", 3, 2));

		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 */

	public static void main4(String[] args) throws InterruptedException {

		ExecutorService executorService = Executors.newFixedThreadPool(2);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Shivam", 3, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Vardhman", 3, 2));

		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-2
	 */

	public static void main3(String[] args) throws InterruptedException {

		ExecutorService executorService = Executors.newFixedThreadPool(1);

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Shivam", 3, 2));

		// Task is to print "Vardhman" for 3 times at interval of 2 second
		executorService.execute(new TaskRunnable("Vardhman", 3, 2));

		executorService.shutdown();
	}
	/*
	 * Output :
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 */

	public static void main2(String[] args) throws InterruptedException {

		System.out.println("Start of Thread : " + Thread.currentThread().getName());
		ExecutorService executorService = Executors.newSingleThreadExecutor();

		/*
		 * Here we are going to start 2 task but as we have only one thread executor,
		 * task will be executed by the same thread one after another
		 */
		// Task is to print "Shivam" for 3 times at interval of 1 second
		executorService.execute(new TaskRunnable("Shivam", 3, 1));

		// Task is to print "Vardhman" for 3 times at interval of 1 second
		executorService.execute(new TaskRunnable("Vardhman", 4, 2));
		executorService.shutdown();

		System.out.println("End of Thread : " + Thread.currentThread().getName());
	}
	/*
	 * Output :
	 * 
	 * Start of Thread : main
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * End of Thread : main
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Shivam : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 * Vardhman : pool-1-thread-1
	 * 
	 */

	public static void main1(String[] args) throws InterruptedException {

		for (int i = 0; i <= 100; i++) {
			(new Thread(new TaskRunnable("hello", 10, 2000))).start();
			Thread.sleep(5000);
		}

		while (true) {
			System.out.println(Thread.currentThread().getName());
			Thread.sleep(5000);
		}
	}

}
